<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('gender');
            $table->string('questionone');
            $table->string('questiontwo');
            $table->string('questionthree');
            $table->string('questionfour');
            $table->string('questionfive');
            $table->string('questionsix');
            $table->string('questionseven');
            $table->string('questioneight');
            $table->string('questionnine');
            $table->string('questionten');
            $table->string('questioneleven');
            $table->string('questiontwelve');
            $table->string('questionthirteen');
            $table->string('questionfourteen');
            $table->string('questionfifteen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
};
