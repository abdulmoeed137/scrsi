<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
    
   


    return view('register');
})->name('home');
// Route::post('lang', function () {
//     if (\Session::has('lang') && \Session::get('lang') == 'en') {
//         \Session::put('lang', 'ar');
//     } else {
//         \Session::put('lang', 'en');
//     }

//     // dd(\App::getLocale());

//     return back();
// })->name('lang');
Route::get('/langswitcher','HomeController@langSwitch')->name('langswitcher');

// ----------------------------------Workshop Certificate-------------------------
Route::get('/workshop-certificate','HomeController@Certificate')->name('certificate');
Route::get('/maleworkshopcertificate/{email}','HomeController@MaleCertificate')->name('maleworkshopcertificate');
Route::get('/femaleworkshopcertificate/{email}','HomeController@FemaleCertificate')->name('femaleworkshopcertificate');
Route::get('/test',function(){
    return view('maleworkshopcertificate');
});
// ----------------------------------Workshop Certificate End-------------------------


Route::get('/registration','HomeController@showregister')->name('register');
Route::post('/store-register','HomeController@storeregister')->name('register.store');
Route::get('/workshop','HomeController@showworkshop')->name('workshop');
Route::post('/store-workshop','HomeController@storeworkshop')->name('workshop.store');
Route::get('/generateallqr','HomeController@generateAllQr');
Route::get('/survey','HomeController@Survey')->name('survey');
Route::post('/store-survey','HomeController@Store_Survey')->name('survey.store');
Route::get('pdfview',array('as'=>'pdfview','uses'=>'HomeController@pdfview'));


Route::get('/login','Auth\LoginController@login')->name('login');
Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\AdminController@ShowLogin')->name('adminlogin');
    Route::post('/login', 'Auth\AdminController@login')->name('checkadminlogin');

    Route::middleware(['auth:admin'])->group(function () {
        Route::get('admin/edit', 'AdminController@edit')->name('admin.edit');
        Route::put('admin/update', 'AdminController@update')->name('admin.update');
        Route::get('', 'AdminController@dashboard')->name('admindashboard');
        Route::post('logout', 'Auth\AdminController@logout')->name('admin.logout');
        Route::get('/registrations','AdminController@Registrations')->name('admin.registrations');
        Route::get('/workshops','AdminController@Workshop')->name('admin.workshop');
        Route::get('/surveys','AdminController@survey')->name('admin.survey');
        Route::post('/sendmanualemail','AdminController@ManualEmail')->name('manualemail');
        Route::get('/preview/{id}','AdminController@PreviewSurvey')->name('admin.previewsurvey');
        
    });

});

Route::get('check',function(){
    return view('certificatepdf');
});

