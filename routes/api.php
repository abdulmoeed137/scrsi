<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/registration/{qrcodeid}','ApiController@userdetail')->name('scanneddata');
Route::get('/registration-qrscan/{userid}','ApiController@qrscan')->name('qrscan');

Route::get('/workshop/{qrcodeid}','ApiController@workshopdetail')->name('workshopscanneddata');
Route::get('/workshop-qrscan/{workshopid}','ApiController@workshopqrscan')->name('workshopqrscan');

