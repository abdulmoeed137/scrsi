<!doctype html>
<html lang="en">
  <head>
    <title>The First Scientific Conference on Labor Market Research, Studies, and Indicators. &raquo; Registration
    </title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('assets/css/new-style.css') }}">


  </head>
  <body style="position: relative;">
    <header class="af-header" style="    width: 80%;
    display: table;
    padding: 15px 0px;">
      <img src="https://scrsi-sa.com/wp-content/uploads/2021/10/logo10.png" alt="The First Scientific Conference on Labor Market Research, Studies, and Indicators." style="    width: 280px;
      float: right;">
    </header>
     <div class="af-content" style="    width: 90%;
     margin: auto;
     text-align: center; margin-top: 30px;">
           <h2 class="top-hd" style="color: #57b89a;font-weight: 700;"><span style="    border-bottom: 2px solid #f49c36;
            padding-bottom: 4px;">لنشكركم على حسن تجاوبكم</span></h2>
      <h2 style="color: #57b89a;font-weight: 700; margin-top: 20px;">شهادة حضور</h2>
      <p style="font-weight: 700;">يشهد المرصد الوطني للعمل وجامعة الأميرة نورة بنت عبدالرحمن</p>
      @if($gender == 'انثى')
          <h2 style="font-size: 30px;font-weight: 600;"> <span style="color: #077b84;">بـأن / </span> {{$name}}</h2>
          @else
          <h2 style="font-size: 30px;font-weight: 600;"> <span style="color: #077b84;">بـأن / </span> {{$name}}</h2>
      @endif

      @if($gender == 'انثى')
          <p style="font-size: 30px;font-weight: 600;">قد حضرت جلسات المؤتمر العلمي الأول لبحوث ودراسات ومؤشرات سوق العمل          </p>
          @else
          <p style="font-size: 30px;font-weight: 600;">قد حضر جلسات المؤتمر العلمي الأول لبحوث ودراسات ومؤشرات سوق العمل</p>
      @endif

      <p  style="font-weight: 700;">وذلك يوم الموافق</p>
      <p style="font-weight: 700;">سائلين له المولى التوفيق والسداد.</p>
           
     </div>
     <div class="or-line" style="height: 100%;;
     width: 40px;
     background: #f49c36;
     position: absolute;
     top: 0;
     left: 80px; z-index: 1;">
     </div>
     <div class="sign-con" style="width: 80%; margin: auto;">
       <h6 style="color: #077b84;    font-weight: 700;">المدير التنفيذي للمرصد الوطني للعمل</h6>
       <h6 style="color: #57b89a;    font-weight: 700;">د. أسامة بن عبدالرحمن الجميلي</h6>
       <img src="{{ asset('assets/img/sign.png') }}" alt="" width="160">
     </div>


     <div style="text-align: center">
      <a href="{{ route('pdfview',['download'=>'pdf','id'=>$id]) }}" style="display: inline-block;margin-top: 14px;background: #f49c36;color: #fff;border-radius: 30px;padding: 12px 44px;font-weight: 700;margin-bottom: 30px;border: 0;">انقر هنا لتحميل شهادتك</a>
    </div>
      <footer class="af-footer" style="background: #268b93;
      width: 100%;
      display: table;
      text-align: right;
      padding: 10px 20px">
      <img src="https://nlo.gov.sa//Content/Uploads/b254504a-db10-4984-b499-e9eb5f79f296.png" alt="" width="30" style="margin-right: 20px;">
      <img src="https://www.hrdf.org.sa/media/img/hrdflogo.svg" alt="" width="82" style="margin-right: 20px;">
      <img src="{{ asset('assets/img/lastl.png') }}" alt="" width="120" style="margin-right: 20px;">
     </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>


