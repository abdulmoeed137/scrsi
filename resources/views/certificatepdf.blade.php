<!doctype html>
<html lang="en">
  <head>
    <title>The First Scientific Conference on Labor Market Research, Studies, and Indicators. &raquo; Registration
    </title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('assets/css/new-style.css') }}">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Tajawal:wght@200;300&display=swap" rel="stylesheet">
    <style>
        @font-face {
            font-family: ff-primary-regular;
            src: url({{ asset('assets/fonts/tajawal.ttf') }});
        }
​
        * {
            box-sizing: border-box;
        }
​
        body {
            font-family: ff-primary-regular !important;
            padding: 0;
            margin: 0;
​
        }
</style>
  </head>
  
  <body style="position: relative; height: 1123px;">
    <header class="af-header" style="    width: 80%;
    display: table;
    padding: 50px 0px; margin: auto;">
      <img src="https://scrsi-sa.com/wp-content/uploads/2021/10/logo10.png" alt="The First Scientific Conference on Labor Market Research, Studies, and Indicators." style="    width: 280px;
      float: right;">
    </header>
     <div class="af-content" style="width: 90%;
     margin: auto;
     text-align: center; margin-top: 30px;">
      <h2 style="font-family: ff-primary-regular;color: #57b89a;font-weight: 700;">شهادة حضور</h2>
      <p style="font-family: ff-primary-regular; font-size: 20px;">يشهد المرصد الوطني للعمل وجامعة الأميرة نورة بنت عبدالرحمن</p>
      @if($gender == 'انثى')
          <h2 style="font-family: ff-primary-regular;font-weight: 600;"><span style="color: #077b84;">بـأن / </span>{{$name}}</h2>
          @else
          <h2 style="font-family: ff-primary-regular;font-weight: 600;"><span style="color: #077b84;">بـأن / </span>{{$name}}</h2>
      @endif

      @if($gender == 'انثى')
          <p style="font-weight: 600;font-family: ff-primary-regular;font-size: 20px;">قد حضرت جلسات المؤتمر العلمي الأول لبحوث ودراسات ومؤشرات سوق العمل          </p>
          @else
          <p style="font-weight: 600;font-family: ff-primary-regular;font-size: 20px;">قد حضر جلسات المؤتمر العلمي الأول لبحوث ودراسات ومؤشرات سوق العمل</p>
      @endif

      <p style="font-weight: 700; font-family: ff-primary-regular;font-size: 20px;">وذلك يوم الموافق</p>
      <p style="font-weight: 700; font-family: ff-primary-regular;font-size: 20px;">سائلين له المولى التوفيق والسداد.</p>

     </div>
     <div  style="    height: 100%;
     width: 40px;
     background: #f49c36;
     position: absolute;
     top: 0;
     left: 50px; z-index: 1;">
     </div>
     <div class="sign-con" style="width: 80%; margin: auto;">
       <h6 style="color: #077b84;font-family: ff-primary-regular;font-size: 20px;">المدير التنفيذي للمرصد الوطني للعمل</h6>
       <h6 style="color: #57b89a;font-family: ff-primary-regular;font-size: 20px;">د. أسامة بن عبدالرحمن الجميلي</h6>
       <img src="https://scrsi-sa.spidertech.dev/assets/img/sign.png" alt="" width="160">
     </div>
     <footer class="af-footer" style=" background: #268b93;
     width: 100%;
     display: table; bottom: 0;left: 0; right: 0;text-align: right; padding: 10px 20px; position: absolute; padding-right: 50px;">
 <img src="{{ asset('assets/img/logof.png') }}" alt="" width="30" style="margin-right: 20px;">
 <img src="https://www.hrdf.org.sa/media/img/hrdflogo.svg" alt="" width="82" style="margin-right: 20px;">
      <img src="{{ asset('assets/img/lastl.png') }}" alt="" width="120" style="margin-right: 20px; padding-right: 20px;">
     </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>