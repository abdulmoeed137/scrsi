@extends('layouts.admin')

@section('custom-css')
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{ URL::asset('admin-assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet"
    href="{{ URL::asset('admin-assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
@endsection
<style>

</style>
@section('section-content')

<section class="content mt-5">
  <div class="row">
      <div class="col-12">
          <div class="card mt-3">
              <div class="card-header" style="text-align: right">
                  <div class="d-flex justify-content-between align-items-center">
                      <h3 class="card-title">Preview</h3>
                  </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body" style="text-align: right">
                  <div class="table-responsive">
                        <h6>الاسم: {{$surveypreview->name}}</h6>
                        <h6>الجنس: {{$surveypreview->gender}}</h6>
                  </div>
              </div>
              <div class="card-footer" style="text-align: right">
                <p style="">سؤال ۱</p>
                <h6 style="font-weight: bold;">المدة المخصصة للجلسات كافية</h6>
                <h6>{{$surveypreview->questionone}}</h6>
                <hr>
                <p>سؤال ۲</p>
                <h6 style="font-weight: bold;">تَركّز النقاش في الجلسات على الموضوعات المحددة</h6>
                <h6>{{$surveypreview->questiontwo}}</h6>
                <hr>

                <p>سؤال ۳</p>
                <h6 style="font-weight: bold;">ساهمت الجلسات في إثراء معلوماتي حول موضوع المؤتمر</h6>
                <h6>{{$surveypreview->questionthree}}</h6>
                <hr>

                <p>سؤال ٤</p>
                <h6 style="font-weight: bold;">تم إدارة الجلسات بفاعلية</h6>
                <h6>{{$surveypreview->questionfour}}</h6>
                <hr>
                <p>سؤال ٥</p>
                <h6 style="font-weight: bold;">التوصيات التي طُرِحت في المؤتمر قابلة للتطبيق</h6>
                <h6>{{$surveypreview->questionfive}}</h6>
                <hr>
                <p>سؤال ٦</p>
                <h6 style="font-weight: bold;">تنظيم المؤتمر مناسب</h6>
                <h6>{{$surveypreview->questionsix}}</h6>
                <hr>
                <p>سؤال ۷</p>
                <h6 style="font-weight: bold;">إصدارات ومطبوعات المؤتمر ثرية وكافية</h6>
                <h6>{{$surveypreview->questionseven}}</h6>
                <hr>
                <p>سؤال ۸</p>
                <h6 style="font-weight: bold;">البرنامج الزمني لفعاليات المؤتمر مناسب</h6>
                <h6>{{$surveypreview->questioneight}}</h6>
                <hr>
                <p>سؤال ۹</p>
                <h6 style="font-weight: bold;">التقنيات المستخدمة في المؤتمر مناسبة</h6>
                <h6>{{$surveypreview->questionnine}}</h6>
                <hr>
                <p>سؤال ۱۰</p>
                <h6 style="font-weight: bold;">التغطية الإعلامية للمؤتمر مناسبة</h6>
                <h6>{{$surveypreview->questionten}}</h6>
                <hr>
                <p>سؤال ۱۱</p>
                <h6 style="font-weight: bold;">طريقة التسجيل والمشاركة في المؤتمر مناسبة</h6>
                <h6>{{$surveypreview->questioneleven}}</h6>
                <hr>
                <p>سؤال ۱۲</p>
                <h6 style="font-weight: bold;">إجراءات الحصول على شهادات الحضور والمشاركة مناسب</h6>
                <h6>{{$surveypreview->questiontwelve}}</h6>
                <hr>
                <p>سؤال ۱۳</p>
                <h6 style="font-weight: bold;">موقع المؤتمر مناسب</h6>
                <h6>{{$surveypreview->questionthirteen}}</h6>
                <hr>
                <p>سؤال ۱٤</p>
                <h6 style="font-weight: bold;">إجمالاً، أنا راض عن المؤتمر</h6>
                <h6>{{$surveypreview->questionfourteen}}</h6>
                <hr>
                <p>سؤال ۱٥</p>
                <h6 style="font-weight: bold;">إذا كان لديك ملاحظات أخرى، نأمل تدوينها</h6>
                <h6>{{$surveypreview->questionfifteen}}</h6>
                <hr>
              </div>
              <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
      <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<script>
    @if(Session::has('message'))
        toastr.options = {
            "closeButton": true,
            "progressBar": true
        }
        toastr.success("{{ session('message') }}");
        @endif
</script>
@endsection

@section('custom-script')
<!-- DataTables -->
<script src="{{ URL::asset('admin-assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('admin-assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
</script>
<script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
</script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.colVis.min.js"></script>
<!-- --------------------------DataTable Start ----------------- -->
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> --}}
<!-- --------------------------DataTable End ----------------- -->

<script>
    $(document).ready(function() {
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": false,
                "columnDefs": [{
                    "orderable": true,
                    "targets": 0
                }],
                'aaSorting': [
                    [0, 'desc']
                ],
                dom: 'Bfrtip',
                
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'register',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    // {
                    //     extend: 'pdfHtml5',
                    //     title: 'register',
                    //     orientation: 'landscape',
                    //     pageSize: 'LEGAL',
                    //     exportOptions: {
                    //         // columns: [0,1,2,3,4,5,6,7,8,9,10],
                    //         columns: ':visible',
                            
                    //     }
                    // },
                    'colvis'
                ]
            });
            
        });
</script>
<script>
    $('#acceptbtn').click(function(e) {
            // alert();
            var id = $(this).val();
            // alert(id);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // alert();
            $.ajax({
                url: "/ajax-accept",
                type: "POST",
                dataType: 'JSON',
                data: {
                    id: id,
                },
                // alert(),
                success: function(response) {
                    console.log(response);
                    if (response) {
                        $('.success').text(response.success);
                        $("#ajaxform")[0].reset();
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        })
</script>
<script>
    $(document).on('click','#emailsenderbtn',function(){
        // alert('dvhjkz');
        var arr1 = [];
        let _token   = $('meta[name="csrf-token"]').attr('content');
$('.senderEmail').each(function(e,i){
    // alert(e);
    
if($(i).is(':checked')){
    // alert($(i).val());
    arr1.push($(i).val());
}
});
if(arr1.length > 0){
    $.ajax({
               url: "{{route('manualemail')}}",
               type: 'post',
               data: {arr1:arr1,_token:_token},
               success: function(response){
                toastr.success(response.message);
                  dataTable.ajax.reload();
               }
            });
}
console.log(arr1);
    });
</script>
<script>
    $(document).on('click','.qrcodemodal',function(event){
        event.preventDefault();
    var id = $(this).data('id');
    document.getElementById("imgtagofmodal").src="{{URL::asset('storage/qr-code/workshop')}}"+id+'.png';
    document.getElementById("downloadbtnofmodal").href="{{URL::asset('storage/qr-code/workshop')}}"+id+'.png';
    console.log(id);
    });
</script>
@endsection