@extends('layouts.admin')

@section('custom-css')
<!-- overlayScrollbars -->
<link rel="stylesheet" href="{{ URL::asset('admin-assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
@endsection

@section('section-content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Admin Dashboard</h1>
            </div><!-- /.col -->
            {{-- <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col --> --}}
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            {{-- <div class="col-6 col-sm-2 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Submissions</span>
                        <span class="info-box-number">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div> --}}
            <!-- /.col -->

            <!-- fix for small devices only -->
            {{-- <div class="clearfix hidden-md-up"></div> --}}
            <!-- /.col -->

            {{-- <div class="col-6 col-sm-2 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Speakers</span>
                        <span class="info-box-number">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div> --}}
            <!-- /.col -->
            <!-- /.col -->
            {{-- <div class="col-6 col-sm-2 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Sessions</span>
                        <span class="info-box-number">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div> --}}
            <!-- /.col -->
            <!-- /.col -->
            {{-- <div class="col-6 col-sm-2 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Writers</span>
                        <span class="info-box-number">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div> --}}
            <!-- /.col -->
            <div class="col-12 col-sm-12 col-md-12">
                <div class="info-box mb-3">
                    <div class="info-box-content">
                        {{-- <span class="info-box-text">Users Signup Graph</span> --}}

                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="info-box-number">Total Registrations of Event
                                        ({{$registrations}})</span>

                                    <span class="info-box-number"><canvas id="userChart"
                                            class="rounded shadow"></canvas></span>
                                </div>

                                <div class="col-md-6">
                                    <span class="info-box-number">Total Registrations of Workshop
                                        ({{$workshopregistrations}})</span>

                                    <span class="info-box-number"><canvas id="userChart2"
                                            class="rounded shadow"></canvas></span>
                                </div>
                            </div>
                        </div>
                        <div>

                            <h1></h1>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="info-box mb-3">
                                        <span class="info-box-icon bg-warning elevation-1"><i
                                                class="fas fa-users"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Total QR Scanned for Event</span>
                                            <span class="info-box-number">{{$registerqrscanned}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="info-box mb-3">
                                        <span class="info-box-icon bg-info elevation-1"><i
                                                class="fas fa-users"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Total QR Scanned for Workshop</span>
                                            <span class="info-box-number">{{$workshopqrscanned}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <span class="info-box-number">Total Survey Submitted ({{$surveys}})</span>

                                <span class="info-box-number"><canvas id="userChart3"
                                        class="rounded shadow"></canvas></span>
                            </div>
                            {{-- <div class="col-md-6">
                                <span class="info-box-number">Total Registrations of Survey On Gender
                                    ({{$surveys}})</span>

                                <span class="info-box-number"><canvas id="userChart4"
                                        class="rounded shadow"></canvas></span>
                            </div> --}}


                        </div>
                        <h1></h1>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="info-box mb-3">
                                        <span class="info-box-icon bg-success elevation-1"><i
                                                class="fas fa-users"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Survey Submitted by Male</span>
                                            <span class="info-box-number">{{$malesurvey}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="info-box mb-3">
                                        <span class="info-box-icon bg-danger elevation-1"><i
                                                class="fas fa-users"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Survey Submitted by Female</span>
                                            <span class="info-box-number">{{$femalesurvey}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
</section>
<!-- /.content -->
@endsection

@section('custom-script')
<!-- overlayScrollbars -->
<script src="{{ URL::asset('admin-assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ URL::asset('admin-assets/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ URL::asset('admin-assets/plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ URL::asset('admin-assets/plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ URL::asset('admin-assets/plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ URL::asset('admin-assets/plugins/chart.js/Chart.min.js') }}"></script>
<!-- PAGE SCRIPTS -->
<script src="{{ URL::asset('admin-assets/dist/js/pages/dashboard2.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<!-- CHARTS -->
<script>
    var ctx = document.getElementById('userChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
// The data for our dataset

data: {
            labels:  {!!json_encode($chart->labels)!!} ,
            datasets: [
                {
                    label: 'User Registered Per Day',
                    backgroundColor: {!! json_encode($chart->colours)!!} ,
                    fill : false,
                    data:  {!! json_encode($chart->dataset)!!} ,
                },
            ]
        },

// Configuration options go here
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function(value) {if (value % 1 === 0) {return value;}}
                    },
                    scaleLabel: {
                        display: false
                    }
                }]
            },
            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    fontColor: '#122C4B',
                    fontFamily: "'Muli', sans-serif",
                    padding: 25,
                    boxWidth: 25,
                    fontSize: 14,
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 0,
                    bottom: 10
                }
            }
        }
    });
</script>
{{-- ----CHART 2------- --}}
<script>
    var ctx = document.getElementById('userChart2').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
// The data for our dataset

data: {
            labels:  {!!json_encode($chart2->labels)!!} ,
            datasets: [
                {
                    label: 'Workshop Registration',
                    backgroundColor: {!! json_encode($chart2->colours)!!} ,
                    fill : false,
                    data:  {!! json_encode($chart2->dataset)!!} ,
                },
            ]
        },

// Configuration options go here
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function(value) {if (value % 1 === 0) {return value;}}
                    },
                    scaleLabel: {
                        display: false
                    }
                }]
            },
            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    fontColor: '#122C4B',
                    fontFamily: "'Muli', sans-serif",
                    padding: 25,
                    boxWidth: 25,
                    fontSize: 14,
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 0,
                    bottom: 10
                }
            }
        }
    });
</script>
{{-- ----CHART 3------- --}}
<script>
    var ctx = document.getElementById('userChart3').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
// The data for our dataset

data: {
            labels:  {!!json_encode($chart3->labels)!!} ,
            datasets: [
                {
                    label: 'Survey Registration',
                    backgroundColor: {!! json_encode($chart3->colours)!!} ,
                    fill : false,
                    data:  {!! json_encode($chart3->dataset)!!} ,
                },
            ]
        },

// Configuration options go here
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function(value) {if (value % 1 === 0) {return value;}}
                    },
                    scaleLabel: {
                        display: false
                    }
                }]
            },
            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    fontColor: '#122C4B',
                    fontFamily: "'Muli', sans-serif",
                    padding: 25,
                    boxWidth: 25,
                    fontSize: 14,
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 0,
                    bottom: 10
                }
            }
        }
    });
</script>
{{-- ----CHART 4------- --}}
<script>
    var ctx = document.getElementById('userChart4').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
// The data for our dataset

data: {
            labels:  {!!json_encode($chart4->labels)!!} ,
            datasets: [
                {
                    label: 'Survey Registration',
                    backgroundColor: {!! json_encode($chart4->colours)!!} ,
                    fill : false,
                    data:  {!! json_encode($chart4->dataset)!!} ,
                },
            ]
        },

// Configuration options go here
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function(value) {if (value % 1 === 0) {return value;}}
                    },
                    scaleLabel: {
                        display: false
                    }
                }]
            },
            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    fontColor: '#122C4B',
                    fontFamily: "'Muli', sans-serif",
                    padding: 25,
                    boxWidth: 25,
                    fontSize: 14,
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 0,
                    bottom: 10
                }
            }
        }
    });
</script>
@endsection