<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ app()->getLocale() == 'en' ? 'ltr' : 'rtl' }}">

<head>
    <title>The First Scientific Conference on Labor Market Research, Studies, and Indicators. &raquo; Registration
    </title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/fav.png') }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/new-style.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/style.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/png" href="https://scrsi-sa.com/wp-content/uploads/2021/10/favicon.png" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/ltr.css?ver=524" rel="stylesheet" type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/screen.css?ver=648" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/icons.css?ver=927" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/lightview.css?ver=486" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/shape_ltr.css?ver=145" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/phone.css?ver=420" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/fonts.css?ver=859" rel="stylesheet"
        type="text/css" />
    {{-- style --}}
    @if(\App::getLocale()=="en")
    <link rel='stylesheet' id='sweetalert2-css'
    href='https://scrsi-sa.com/wp-content/plugins/user-registration/assets/css/sweetalert2/sweetalert2.min.css?ver=10.16.7'
    type='text/css' media='all' />
<link rel='stylesheet' id='user-registration-general-css'
    href='https://scrsi-sa.com/wp-content/plugins/user-registration/assets/css/user-registration.css?ver=2.1.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='user-registration-smallscreen-css'
    href='https://scrsi-sa.com/wp-content/plugins/user-registration/assets/css/user-registration-smallscreen.css?ver=2.1.1'
    type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='user-registration-my-account-layout-css'
    href='https://scrsi-sa.com/wp-content/plugins/user-registration/assets/css/my-account-layout.css?ver=2.1.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css' href='https://scrsi-sa.com/wp-includes/css/dashicons.min.css?ver=5.9.3'
    type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'
    href='https://scrsi-sa.com/wp-includes/css/dist/block-library/style.min.css?ver=5.9.3' type='text/css'
    media='all' />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@else
 {{-- tyhbtyhn --}}
 <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/style.css" rel="stylesheet" type="text/css" />
 <link rel="shortcut icon" type="image/png" href="https://scrsi-sa.com/wp-content/uploads/2021/10/favicon.png" />
 <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/style.css?ver=574" rel="stylesheet" type="text/css" />
 <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/screen.css?ver=890" rel="stylesheet" type="text/css" />
 <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/icons.css?ver=860" rel="stylesheet" type="text/css" />
 <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/lightview.css?ver=488" rel="stylesheet" type="text/css" />
 <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/shape.css?ver=502" rel="stylesheet" type="text/css" />
 <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/phone.css?ver=801" rel="stylesheet" type="text/css" />
 <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/fonts.css?ver=479" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @endif
    

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-T7104ZWXQV"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-T7104ZWXQV');
</script>

    @toastr_css
</head>

<body>
    <div id="top">
        <div class="width">
            <div class="icon_menu_open"></div>
            <div class="lang">
                <a class="lan-btn" href="{{route('home')}}?lang={{ \App::getLocale()=='en'?'ar':'en' }}">{{ \App::getLocale()=='en'?'ع':'EN' }}</a>
                
                
            </div>
            <!--End Lang-->
            <ul class="s_contact">
                <li class="phone"><a href="tel:0000000000">
                        <font dir="ltr">0000000000</font>
                    </a></li>
                <li class="mail"><a href="mailto:info@scrsi-sa.com">info@scrsi-sa.com</a></li>
                <li class="mapi"><span>{{__('Airport Road, King Khalid International Airport')}}</span></li>
            </ul>
            <!--End S_contact-->
        </div>
        <!--End Width-->
    </div>
    <div id="header">
        <div class="width">
            <div class="logo">
                <a href="https://scrsi-sa.com/en">
                    <img src="https://scrsi-sa.com/wp-content/uploads/2021/10/logo10.png"
                        alt="The First Scientific Conference on Labor Market Research, Studies, and Indicators.">
                </a>
            </div>
            <!--End Logo-->
            <div class="head_left">
                <div class="social">
                    <span>{{__('Social Media')}}</span>
                    <ul>
                        <li class="facebook"><a href="1"></a></li>
                        <li class="twitter"><a href="2"></a></li>
                        <li class="google"><a href="3"></a></li>
                        <li class="snapchat"><a href="6"></a></li>
                        <li class="linkedin"><a href="5"></a></li>
                        <li class="instagram"><a href="4"></a></li>
                    </ul>
                </div>
                <!--End Social-->
                <ul id="menu-menu" class="nav">
                    @if(\App::getLocale()=="en")
                    <li class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-home"><a
                        href="https://scrsi-sa.com/en/">{{__('Main')}}</a></li>
                <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                        href="https://scrsi-sa.com/en/%d8%b9%d9%86-%d8%a7%d9%84%d9%85%d8%a4%d8%aa%d9%85%d8%b1/">{{__('About Us')}}</a></li>
                <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                        href="https://scrsi-sa.com/en/workshopregisteration/">{{__('Workshop Registeratio')}}n</a></li>
                <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                        href="https://scrsi-sa.com/en/%d9%85%d8%ad%d8%a7%d9%88%d8%b1-%d8%a7%d9%84%d9%85%d8%a4%d8%aa%d9%85%d8%b1/">{{__('Conference Themes')}}</a></li>
                {{-- <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                        href="https://scrsi-sa.com/en/%d8%a8%d8%b1%d9%86%d8%a7%d9%85%d8%ac-%d8%a7%d9%84%d9%85%d8%a4%d8%aa%d9%85%d8%b1/">conference
                        speakers</a></li> --}}
                        @else
                        <li class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-home"><a
                            href="https://scrsi-sa.com/">الرئيسية</a></li>
                    <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="https://scrsi-sa.com/%d8%b9%d9%86-%d8%a7%d9%84%d9%85%d8%a4%d8%aa%d9%85%d8%b1/">عن المؤتمر</a></li>
                    <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="https://scrsi-sa.com/workshopregisteration/">التسجيل في ورش العمل</a></li>
                    <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="https://scrsi-sa.com/%d9%85%d8%ad%d8%a7%d9%88%d8%b1-%d8%a7%d9%84%d9%85%d8%a4%d8%aa%d9%85%d8%b1/">محاور المؤتمر</a></li>
                   
                    @endif
                    
                </ul>
            </div>
            <!--End Left-->
        </div>
        <!--End Width-->
    </div>
<div class="mainiii ">
    <div id="precemp">
        <div class="width">
            <ul class="precemp">
                <li><a href="https://scrsi-sa.com/en">homepage</a></li>
                <li><span>Registration</span></li>
            </ul>
        </div>
        <!--End Width-->
    </div>
  <div class="cener-box">
    <div class="wraps_is">
        <div class="text-center">
            <h2 class="reg-heading">
                You are awarded with the Certificate of Workshop at the Scientific Conference For Labor Market Research 2022
            </h2>
           
        </div>
    </div>
  </div>
</div>

<br>

<div class="container">
    <div class="row text-center">
        <div class="col-md-6">
            <h6>Males can get their Certificate from here</h6>

            <a href="{{route('maleworkshopcertificate','nimrah@gmail.com')}}" style="    width: 90%;
                    display: block;
                    padding: 14px 0px;
                    margin-bottom: 20px;
                    margin: auto;
                    margin-bottom: 40px;
                    color: white;
                    text-decoration: none;
                    margin-top: 0px;
                    border-radius: 3px;"> <img style="    background: #007781;
    padding: 9px 11px;
    border-radius: 50%;" src="https://imgur.com/tZl6r1Z.png" alt="" width="53"></a>



            {{-- <a class="re-btn" href="{{route('maleworkshopcertificate','nimrah@gmail.com')}}"> Download Male Certificate  </a> --}}
        </div>
        
        <div class="col-md-6 mt-2">
            <h6>Females can get their Certificate from here </h6>


            <a href="{{route('femaleworkshopcertificate','nimrah@gmail.com')}}" style="    width: 90%;
            display: block;
            padding: 14px 0px;
            margin-bottom: 20px;
            margin: auto;
            margin-bottom: 40px;
            color: white;
            text-decoration: none;
            margin-top: 0px;
            border-radius: 3px;"> <img style="    background: #007781;
padding: 9px 11px;
border-radius: 50%;" src="https://imgur.com/tZl6r1Z.png" alt="" width="53"></a>



            {{-- <a class="re-btn" href="{{route('femaleworkshopcertificate','nimrah@gmail.com')}}"> Download Female Certificate  </a> --}}
        </div>
        
    </div>
</div>
<br>
    
    


    <div id="bottom">
        <div class="width">
            <div class="copyrights">all rights reserved to
                <span>The First Scientific Conference For Labor Market Research 2021 </span> 2021
            </div>
            <!--End Copyright-->
            <div class="design">
            </div>
            <div style="clear:both;"></div>
            <!--End Both-->
        </div>
        <!--End Width-->
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>


<script type='text/javascript' src='https://scrsi-sa.com/wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
<script type='text/javascript' src='https://scrsi-sa.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
<script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/jquery.cycle2.js?ver=1.0.0' id='cycle-js'></script>
<script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/jquery.cycle2.tile.js?ver=1.0.0' id='cycle_tile-js'></script>
<script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/slick.js?ver=1.0.0' id='slick-js'></script>
<script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/lightview.js?ver=1.0.0' id='lightview-js'></script>
<script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/code.js?ver=1.0.0' id='code-js'></script>
<script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/shape.js?ver=1.0.0' id='shape-js'></script>

<script>
    @if(Session::has('message'))
    toastr.options = {
        "closeButton": true,
        "progressBar": true
    }
    toastr.success("{{ session('message') }}");
    @endif
    </script>

@jquery
@toastr_js
@toastr_render
</body>




</html>