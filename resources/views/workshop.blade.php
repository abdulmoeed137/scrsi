<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ app()->getLocale() == 'en' ? 'ltr' : 'rtl' }}">

<head>
    <title>The First Scientific Conference on Labor Market Research, Studies, and Indicators. &raquo; Registration
    </title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/fav.png') }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/new-style.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/style.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/png" href="https://scrsi-sa.com/wp-content/uploads/2021/10/favicon.png" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/ltr.css?ver=524" rel="stylesheet" type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/screen.css?ver=648" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/icons.css?ver=927" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/lightview.css?ver=486" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/shape_ltr.css?ver=145" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/phone.css?ver=420" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/fonts.css?ver=859" rel="stylesheet"
        type="text/css" />
    {{-- style --}}
    @if(\App::getLocale()=="en")
    <link rel='stylesheet' id='sweetalert2-css'
        href='https://scrsi-sa.com/wp-content/plugins/user-registration/assets/css/sweetalert2/sweetalert2.min.css?ver=10.16.7'
        type='text/css' media='all' />
    <link rel='stylesheet' id='user-registration-general-css'
        href='https://scrsi-sa.com/wp-content/plugins/user-registration/assets/css/user-registration.css?ver=2.1.1'
        type='text/css' media='all' />
    <link rel='stylesheet' id='user-registration-smallscreen-css'
        href='https://scrsi-sa.com/wp-content/plugins/user-registration/assets/css/user-registration-smallscreen.css?ver=2.1.1'
        type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='user-registration-my-account-layout-css'
        href='https://scrsi-sa.com/wp-content/plugins/user-registration/assets/css/my-account-layout.css?ver=2.1.1'
        type='text/css' media='all' />
    <link rel='stylesheet' id='dashicons-css' href='https://scrsi-sa.com/wp-includes/css/dashicons.min.css?ver=5.9.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='wp-block-library-css'
        href='https://scrsi-sa.com/wp-includes/css/dist/block-library/style.min.css?ver=5.9.3' type='text/css'
        media='all' />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @else
    {{-- tyhbtyhn --}}
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/style.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/png" href="https://scrsi-sa.com/wp-content/uploads/2021/10/favicon.png" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/style.css?ver=574" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/screen.css?ver=890" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/icons.css?ver=860" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/lightview.css?ver=488" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/shape.css?ver=502" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/phone.css?ver=801" rel="stylesheet"
        type="text/css" />
    <link href="https://scrsi-sa.com/wp-content/themes/mo2tmr2/css/fonts.css?ver=479" rel="stylesheet"
        type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @endif

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-T7104ZWXQV"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-T7104ZWXQV');
</script>


    @toastr_css
</head>

<body>
    <div id="top">
        <div class="width">
            <div class="icon_menu_open"></div>
            <div class="lang">
                <a class="lan-btn" href="{{route('workshop')}}?lang={{ \App::getLocale()=='en'?'ar':'en' }}">{{
                    \App::getLocale()=='en'?'ع':'EN' }}</a>


            </div>
            <!--End Lang-->
            <ul class="s_contact">
                <li class="phone"><a href="tel:0000000000">
                        <font dir="ltr">0000000000</font>
                    </a></li>
                <li class="mail"><a href="mailto:info@scrsi-sa.com">info@scrsi-sa.com</a></li>
                <li class="mapi"><span>{{__('Airport Road, King Khalid International Airport')}}</span></li>
            </ul>
            <!--End S_contact-->
        </div>
        <!--End Width-->
    </div>
    <div id="header">
        <div class="width">
            <div class="logo">
                <a href="https://scrsi-sa.com/en">
                    <img src="https://scrsi-sa.com/wp-content/uploads/2021/10/logo10.png"
                        alt="The First Scientific Conference on Labor Market Research, Studies, and Indicators.">
                </a>
            </div>
            <!--End Logo-->
            <div class="head_left">
                <div class="social">
                    <span>{{__('Social Media')}}</span>
                    <ul>
                        <li class="facebook"><a href="1"></a></li>
                        <li class="twitter"><a href="2"></a></li>
                        <li class="google"><a href="3"></a></li>
                        <li class="snapchat"><a href="6"></a></li>
                        <li class="linkedin"><a href="5"></a></li>
                        <li class="instagram"><a href="4"></a></li>
                    </ul>
                </div>
                <!--End Social-->
                <ul id="menu-menu" class="nav">
                    @if(\App::getLocale()=="en")
                    <li class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-home"><a
                            href="https://scrsi-sa.com/en/">{{__('Main')}}</a></li>
                    <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="https://scrsi-sa.com/en/%d8%b9%d9%86-%d8%a7%d9%84%d9%85%d8%a4%d8%aa%d9%85%d8%b1/">{{__('About
                            Us')}}</a></li>
                    <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="https://scrsi-sa.com/en/workshopregisteration/">{{__('Workshop Registeration')}}n</a>
                    </li>
                    <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="https://scrsi-sa.com/en/%d9%85%d8%ad%d8%a7%d9%88%d8%b1-%d8%a7%d9%84%d9%85%d8%a4%d8%aa%d9%85%d8%b1/">{{__('Conference
                            Themes')}}</a></li>
                    {{-- <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="https://scrsi-sa.com/en/%d8%a8%d8%b1%d9%86%d8%a7%d9%85%d8%ac-%d8%a7%d9%84%d9%85%d8%a4%d8%aa%d9%85%d8%b1/">conference
                            speakers</a></li> --}}
                    @else
                    <li class=" menu-item menu-item-type-custom menu-item-object-custom menu-item-home"><a
                            href="https://scrsi-sa.com/">الرئيسية</a></li>
                    <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="https://scrsi-sa.com/%d8%b9%d9%86-%d8%a7%d9%84%d9%85%d8%a4%d8%aa%d9%85%d8%b1/">عن
                            المؤتمر</a></li>
                    <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="https://scrsi-sa.com/workshopregisteration/">التسجيل في ورش العمل</a></li>
                    <li class=" menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="https://scrsi-sa.com/%d9%85%d8%ad%d8%a7%d9%88%d8%b1-%d8%a7%d9%84%d9%85%d8%a4%d8%aa%d9%85%d8%b1/">محاور
                            المؤتمر</a></li>

                    @endif

                </ul>
            </div>
            <!--End Left-->
        </div>
        <!--End Width-->
    </div>
    <div class="mainiii">
        <div id="precemp">
            <div class="width">
                <ul class="precemp">
                    <li><a href="https://scrsi-sa.com/en">homepage</a></li>
                    <li><span>Workshop Registration</span></li>
                </ul>
            </div>
            <!--End Width-->
        </div>
        <div class="cener-box">

            <div style="text-align: center;">
                <a target="_blank" href="https://scrsi-sa.com/wp-content/uploads/2022/05/ورش-العمل-المصاحبة-للمؤتمر-العلمي-الأول-لبحوث-ودراسات-سوق-العمل.pdf" style="display: inline-block;
                   display: inline-block;
    margin-top: 14px;
    background: #f49c36;
    color: #fff;
    border-radius: 30px;
    padding: 0px 35px;
    font-weight: 700;
    margin-bottom: 5px;
    line-height: 1;
    padding-bottom: 15px;
    padding-top: 15px;
    margin-top: 30px;" rel="noopener">
            {{__('Workshop schedule')}}
            </a>
                </div>
            <div class="wraps_is">
                <div class="postin_txt">
                    <h2 class="reg-heading">{{__('Workshop Registration')}}</h2>
                    <form action="{{ route('workshop.store') }}" method="POST" class="reg-form">
                        @CSRF
                        <div class="row">
    
                            <div class="col-md-12">
                                <label for="">{{ __('Name') }} <span style="color: red;">*</span></label>
                                <input name="name" class="@error('name') is-invalid @enderror" value="{{ old('name') }}"
                                    placeholder="{{ \App::getLocale() == 'en' ? 'Write your answer here...' : 'اكتب إجابتك هنا ...' }}"
                                    type="text">
                                @error('name')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>
                                    {{ $message }}</span>
                                @enderror
                            </div>
    
    
    
    
                            @if(\App::getLocale()=="en")
                            <div class="col-md-12">
                                <label for="studentstatus">{{__('Student Status')}}</label>
    
                                <select name="studentstatus" class="@error('studentstatus') is-invalid @enderror" value="{{ old('studentstatus') }}"
                                    id="studentstatus">
                                    <option value="Select Option" selected="selected">{{\App::getLocale()=="en"?"Select Option":"حدد الخيار"}}</option>
                                    <option value="Student">{{\App::getLocale()=="en"?"Student":"طالب/ة"}}</option>
                                    <option value="Graduate">{{\App::getLocale()=="en"?"Graduate":"خريج/ة"}}</option>
    
                                </select>
                                @error('studentstatus')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>
                                    {{ $message }}</span>
                                @enderror
                            </div>
                            @else
                            <div class="col-md-12">
                                <label for="studentstatus">{{__('Student Status')}}</label>
    
                                <select name="studentstatus" class="@error('studentstatus') is-invalid @enderror" value="{{ old('studentstatus') }}"
                                    id="studentstatus">
                                    <option value="Select Option" selected="selected">{{\App::getLocale()=="en"?"Select Option":"حدد الخيار"}}</option>
                                    <option value="Student">{{\App::getLocale()=="en"?"Student":"طالب/ة"}}</option>
                                    <option value="Graduate">{{\App::getLocale()=="en"?"Graduate":"خريج/ة"}}</option>
    
                                </select>
                                @error('studentstatus')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>
                                    {{ $message }}</span>
                                @enderror
                            </div>
                            @endif
                            
                            <div class="col-md-12">
                                <label for="">{{ __('Email') }} <span style="color: red;">*</span></label>
                                <input name="email" class="@error('email') is-invalid @enderror" value="{{ old('email') }}"
                                    placeholder="{{ \App::getLocale() == 'en' ? 'Write your answer here...' : 'اكتب إجابتك هنا ...' }}"
                                    type="email">
                                @error('email')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{ $message }}</span>
                                @enderror
                            </div>
                            @if(\App::getLocale()=="en")
                            <div class="col-md-12">
                                <label for="">{{ __('University') }} <span style="color: red;">*</span></label>
                                <select name="university" class="@error('university') is-invalid @enderror" value="{{ old('university') }}" id="university">
                                    <option value="Select Option" selected="selected">{{\App::getLocale()=="en"?"Select Option":"حدد الخيار"}}</option>
                                    <option value="Umm Al-Qura University">{{\App::getLocale()=="en"?"Umm Al-Qura University":"جامعة أم القرى"}}</option>
                                    <option value="Islamic University of Madinah">{{\App::getLocale()=="en"?"Islamic University of Madinah":"الجامعة الإسلامية"}}</option>
                                    <option value="Imam Muhammad bin Saud Islamic University">{{\App::getLocale()=="en"?"Imam Muhammad bin Saud Islamic University":"جامعة الإمام محمد بن سعود الإسلامية"}}</option>
                                    <option value="King Saud University">{{\App::getLocale()=="en"?"King Saud University":"جامعة الملك سعود"}}</option>
                                    <option value="King Abdulaziz University">{{\App::getLocale()=="en"?"King Abdulaziz University":"جامعة الملك عبدالعزيز"}}</option>
                                    <option value="King Fahd University of Petroleum and Minerals">{{\App::getLocale()=="en"?"King Fahd University of Petroleum and Minerals":"جامعة الملك فهد للبترول والمعادن"}}</option>
                                    <option value="King Faisal University">{{\App::getLocale()=="en"?"King Faisal University":"جامعة الملك فيصل"}}</option>
                                    <option value="King Khalid University">{{\App::getLocale()=="en"?"King Khalid University":"جامعة الملك خالد"}}</option>
                                    <option value="Qassim University">{{\App::getLocale()=="en"?"Qassim University":"جامعة القصيم"}}</option>
                                    <option value="Taibah University">{{\App::getLocale()=="en"?"Taibah University":"جامعة أم القرى"}}</option>
                                    <option value="Taif University">{{\App::getLocale()=="en"?"Taif University":"جامعة الطائف"}}</option>
                                    <option value="University of Ha'il">{{\App::getLocale()=="en"?"University of Ha'il":"جامعة حائل"}}</option>
                                    <option value="Jazan University">{{\App::getLocale()=="en"?"Jazan University":"جامعة جازان"}}</option>
                                    <option value="Jouf University">{{\App::getLocale()=="en"?"Jouf University":"جامعة الجوف"}}</option>
                                    <option value="Al-Baha University">{{\App::getLocale()=="en"?"Al-Baha University":"جامعة الباحة"}}</option>
                                    <option value="University of Tabuk">{{\App::getLocale()=="en"?"University of Tabuk":"جامعة تبوك"}}</option>
                                    <option value="Najran University">{{\App::getLocale()=="en"?"Najran University":"جامعة نجران"}}</option>
                                    <option value="Northern Border University">{{\App::getLocale()=="en"?"Northern Border University":"جامعة الحدود الشمالية"}}</option>
                                    <option value="Princess Nourah Bint Abdul Rahman University">{{\App::getLocale()=="en"?"Princess Nourah Bint Abdul Rahman University":"جامعة الأميرة نورة بنت عبدالرحمن"}}</option>
                                    <option value="King Saud bin Abdulaziz University for Health Sciences">{{\App::getLocale()=="en"?"King Saud bin Abdulaziz University for Health Sciences":"جامعة الملك سعود بن عبدالعزيز للعلوم الصحية"}}</option>
                                    <option value="Imam Abdulrahman bin Faisal University">{{\App::getLocale()=="en"?"Imam Abdulrahman bin Faisal University":"جامعة الإمام عبدالرحمن بن فيصل"}}</option>
                                    <option value="Prince Sattam bin Abdulaziz University">{{\App::getLocale()=="en"?"Prince Sattam bin Abdulaziz University":"جامعة الأمير سطام بن عبدالعزيز"}}</option>
                                    <option value="Shaqra University">{{\App::getLocale()=="en"?"Shaqra University":"جامعة شقراء"}}</option>
                                    <option value="Majmaah University">{{\App::getLocale()=="en"?"Majmaah University":"جامعة المجمعة"}}</option>
                                    <option value="Saudi Electronic University">{{\App::getLocale()=="en"?"Saudi Electronic University":"الجامعة السعودية الإلكترونية"}}</option>
                                    <option value="University of Jeddah">{{\App::getLocale()=="en"?"University of Jeddah":"جامعة جدة"}}</option>
                                    <option value="University of Bisha">{{\App::getLocale()=="en"?"University of Bisha":"جامعة بيشة"}}</option>
                                    <option value="King Abdullah University of Science and Technology">{{\App::getLocale()=="en"?"King Abdullah University of Science and Technology":"جامعة الملك عبدالله للعلوم والتقنية"}}</option>
                                    <option value="University of Hafr Al Batin">{{\App::getLocale()=="en"?"University of Hafr Al Batin":"جامعة حفر الباطن"}}</option>
                                    <option value="Arab Open University">{{\App::getLocale()=="en"?"Arab Open University":"الجامعة العربية المفتوحة"}}</option>
                                    <option value="University of Business and Technology">{{\App::getLocale()=="en"?"University of Business and Technology":"جامعة الأعمال والتكنولوجيا"}}</option>
                                    <option value="Prince Sultan University">{{\App::getLocale()=="en"?"Prince Sultan University":"جامعة الأمير سلطان"}}</option>
                                    <option value="Fahad bin Sultan University">{{\App::getLocale()=="en"?"Fahad bin Sultan University":"جامعة الأمير فهد بن سلطان"}}</option>
                                    <option value="Prince Mohammad bin Fahd University">{{\App::getLocale()=="en"?"Prince Mohammad bin Fahd University":"جامعة الأمير محمد بن فهد"}}
                                    </option>
                                    <option value="University of Prince Mugrin">{{\App::getLocale()=="en"?"University of Prince Mugrin":"جامعة الأمير مقرن بن عبدالعزيز"}}</option>
                                    <option value="Alfaisal University">{{\App::getLocale()=="en"?"Alfaisal University":"جامعة الفيصل"}}</option>
                                    <option value="Al Yamamah University">{{\App::getLocale()=="en"?"Al Yamamah University":"جامعة اليمامة"}}</option>
                                    <option value="Dar Al Uloom University">{{\App::getLocale()=="en"?"Dar Al Uloom University":"جامعة دار العلوم"}}</option>
                                    <option value="Effat University">{{\App::getLocale()=="en"?"Effat University":"جامعة عفت"}}</option>
                                    <option value="Dar Al-Hekma University">{{\App::getLocale()=="en"?"Dar Al-Hekma University":"جامعة دار الحكمة"}}</option>
                                    <option value="Riyadh Elm University">{{\App::getLocale()=="en"?"Riyadh Elm University":"جامعة رياض العلم"}}</option>
                                    <option value="Al Maarefa University">{{\App::getLocale()=="en"?"Al Maarefa University":"جامعة المعرفة"}}</option>
                                    <option value="Mustaqbal University">{{\App::getLocale()=="en"?"Mustaqbal University":"جامعة المستقبل"}}</option>
                                    <option value="Al Asala Colleges">{{\App::getLocale()=="en"?"Al Asala Colleges":"جامعة سليمان الراجحي"}}</option>
                                    <option value="Gulf Colleges">{{\App::getLocale()=="en"?"Gulf Colleges":"كليات الخليج الأهلية"}}</option>
                                    <option value="Sulaiman Al Rajhi University">{{\App::getLocale()=="en"?"Sulaiman Al Rajhi University":"كليات عنيزة الأهلية"}}</option>
                                    <option value="Onaizah Colleges">{{\App::getLocale()=="en"?"Onaizah Colleges":"كلية ابن رشد للعلوم الإدارية"}}</option>
                                    <option value="Ibn Rushd College">{{\App::getLocale()=="en"?"Ibn Rushd College":"كلية ابن سينا الاهلية"}}</option>
                                    <option value="Ibn Sina National College">{{\App::getLocale()=="en"?"Ibn Sina National College":"كلية الباحة الاهلية للعلوم"}}</option>
                                    <option value="Al-Baha Private College of Science">{{\App::getLocale()=="en"?"Al-Baha Private College of Science":"كلية البترجي الطبية للعلوم والتكنولوجيا"}}
                                    </option>
                                    <option value="Batterjee Medical College">{{\App::getLocale()=="en"?"Batterjee Medical College":"كلية الريادة للعلوم الصحية"}}</option>
                                    <option value="Al-Riyada College for Health Science">{{\App::getLocale()=="en"?"Al-Riyada College for Health Science":"كليات الريان الأهلية بالمدينة المنورة"}}</option>
                                    <option value="Al-Rayan College">{{\App::getLocale()=="en"?"Al-Rayan College":"كليات الشرق العربي للدراسات العليا"}}</option>
                                    <option value="Arab East College">{{\App::getLocale()=="en"?"Arab East College":"كلية العناية الطبية"}}</option>
                                    <option value="Inaya Medical Colleges">{{\App::getLocale()=="en"?"Inaya Medical Colleges":"كلية الغد الدولية للعلوم الطبية التطبيقية"}}</option>
                                    <option value="Al Ghad International College for Applied Medical Science">{{\App::getLocale()=="en"?"Al Ghad International College for Applied Medical Science":"كلية الرؤية لطب الأسنان والتمريض بجدة"}}</option>
                                    <option value="Buraydah College">{{\App::getLocale()=="en"?"Buraydah College":"كليات بريدة الأهلية"}}</option>
                                    <option value="Jeddah International College">{{\App::getLocale()=="en"?"Jeddah International College":"كلية سعد للتمريض والعلوم الصحية"}}</option>
                                    <option value="Saad College of Nursing and Allied Health Sciences">{{\App::getLocale()=="en"?"Saad College of Nursing and Allied Health Sciences":"كلية فقيه للعلوم الطبية"}}</option>
                                    <option value="Fakeeh College for Medical Science">{{\App::getLocale()=="en"?"Fakeeh College for Medical Science":"كلية محمد المانع للعلوم الطبية"}}
                                    </option>
                                    <option value="Mihammad Al Mana College for Medical Science">{{\App::getLocale()=="en"?"Mihammad Al Mana College for Medical Science":"جامعة الفيصل – كلية الامير سلطان للإدارة"}}</option>
                                    <option value="Prince Sultan College of Business">{{\App::getLocale()=="en"?"Prince Sultan College of Business":"كلية الشمال للتمريض"}}
                                    </option>
                                    <option value="Vision Colleges in Jeddah">{{\App::getLocale()=="en"?"Vision Colleges in Jeddah":"كلية الرؤية بالرياض"}}</option>
                                    <option value="Vision Colleges in Riyadh">{{\App::getLocale()=="en"?"Vision Colleges in Riyadh":"كلية الفيحاء الأهلية"}}</option>
                                    <option value="Northen College of Nursing">{{\App::getLocale()=="en"?"Northen College of Nursing":"كلية الموسى للعلوم الصحية"}}</option>
                                    <option value="ALFAYHA COLLEGE">{{\App::getLocale()=="en"?"ALFAYHA COLLEGE":"كلية جدة العالمية الأهلية"}}</option>
                                    <option value="ALMOOSA COLLEGE OF HEALTH SCIENCE">{{\App::getLocale()=="en"?"ALMOOSA COLLEGE OF HEALTH SCIENCE":"اخرى"}}
                                    </option>
                                    <option value="Other">{{\App::getLocale()=="en"?"Other":""}}</option>
    
                                </select>
                                @error('university')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{ $message }}</span>
                                @enderror
                            </div>
                            @else
                            <div class="col-md-12">
                                <label for="">{{ __('University') }} <span style="color: red;">*</span></label>
                                <select name="university" class="@error('university') is-invalid @enderror" value="{{ old('university') }}" id="university">
                                    <option value="Select Option" selected="selected">{{\App::getLocale()=="en"?"Select Option":"حدد الخيار"}}</option>
                                    <option value="Umm Al-Qura University">{{\App::getLocale()=="en"?"Umm Al-Qura University":"جامعة أم القرى"}}</option>
                                    <option value="Islamic University of Madinah">{{\App::getLocale()=="en"?"Islamic University of Madinah":"الجامعة الإسلامية"}}</option>
                                    <option value="Imam Muhammad bin Saud Islamic University">{{\App::getLocale()=="en"?"Imam Muhammad bin Saud Islamic University":"جامعة الإمام محمد بن سعود الإسلامية"}}</option>
                                    <option value="King Saud University">{{\App::getLocale()=="en"?"King Saud University":"جامعة الملك سعود"}}</option>
                                    <option value="King Abdulaziz University">{{\App::getLocale()=="en"?"King Abdulaziz University":"جامعة الملك عبدالعزيز"}}</option>
                                    <option value="King Fahd University of Petroleum and Minerals">{{\App::getLocale()=="en"?"King Fahd University of Petroleum and Minerals":"جامعة الملك فهد للبترول والمعادن"}}</option>
                                    <option value="King Faisal University">{{\App::getLocale()=="en"?"King Faisal University":"جامعة الملك فيصل"}}</option>
                                    <option value="King Khalid University">{{\App::getLocale()=="en"?"King Khalid University":"جامعة الملك خالد"}}</option>
                                    <option value="Qassim University">{{\App::getLocale()=="en"?"Qassim University":"جامعة القصيم"}}</option>
                                    <option value="Taibah University">{{\App::getLocale()=="en"?"Taibah University":"جامعة أم القرى"}}</option>
                                    <option value="Taif University">{{\App::getLocale()=="en"?"Taif University":"جامعة الطائف"}}</option>
                                    <option value="University of Ha'il">{{\App::getLocale()=="en"?"University of Ha'il":"جامعة حائل"}}</option>
                                    <option value="Jazan University">{{\App::getLocale()=="en"?"Jazan University":"جامعة جازان"}}</option>
                                    <option value="Jouf University">{{\App::getLocale()=="en"?"Jouf University":"جامعة الجوف"}}</option>
                                    <option value="Al-Baha University">{{\App::getLocale()=="en"?"Al-Baha University":"جامعة الباحة"}}</option>
                                    <option value="University of Tabuk">{{\App::getLocale()=="en"?"University of Tabuk":"جامعة تبوك"}}</option>
                                    <option value="Najran University">{{\App::getLocale()=="en"?"Najran University":"جامعة نجران"}}</option>
                                    <option value="Northern Border University">{{\App::getLocale()=="en"?"Northern Border University":"جامعة الحدود الشمالية"}}</option>
                                    <option value="Princess Nourah Bint Abdul Rahman University">{{\App::getLocale()=="en"?"Princess Nourah Bint Abdul Rahman University":"جامعة الأميرة نورة بنت عبدالرحمن"}}</option>
                                    <option value="King Saud bin Abdulaziz University for Health Sciences">{{\App::getLocale()=="en"?"King Saud bin Abdulaziz University for Health Sciences":"جامعة الملك سعود بن عبدالعزيز للعلوم الصحية"}}</option>
                                    <option value="Imam Abdulrahman bin Faisal University">{{\App::getLocale()=="en"?"Imam Abdulrahman bin Faisal University":"جامعة الإمام عبدالرحمن بن فيصل"}}</option>
                                    <option value="Prince Sattam bin Abdulaziz University">{{\App::getLocale()=="en"?"Prince Sattam bin Abdulaziz University":"جامعة الأمير سطام بن عبدالعزيز"}}</option>
                                    <option value="Shaqra University">{{\App::getLocale()=="en"?"Shaqra University":"جامعة شقراء"}}</option>
                                    <option value="Majmaah University">{{\App::getLocale()=="en"?"Majmaah University":"جامعة المجمعة"}}</option>
                                    <option value="Saudi Electronic University">{{\App::getLocale()=="en"?"Saudi Electronic University":"الجامعة السعودية الإلكترونية"}}</option>
                                    <option value="University of Jeddah">{{\App::getLocale()=="en"?"University of Jeddah":"جامعة جدة"}}</option>
                                    <option value="University of Bisha">{{\App::getLocale()=="en"?"University of Bisha":"جامعة بيشة"}}</option>
                                    <option value="King Abdullah University of Science and Technology">{{\App::getLocale()=="en"?"King Abdullah University of Science and Technology":"جامعة الملك عبدالله للعلوم والتقنية"}}</option>
                                    <option value="University of Hafr Al Batin">{{\App::getLocale()=="en"?"University of Hafr Al Batin":"جامعة حفر الباطن"}}</option>
                                    <option value="Arab Open University">{{\App::getLocale()=="en"?"Arab Open University":"الجامعة العربية المفتوحة"}}</option>
                                    <option value="University of Business and Technology">{{\App::getLocale()=="en"?"University of Business and Technology":"جامعة الأعمال والتكنولوجيا"}}</option>
                                    <option value="Prince Sultan University">{{\App::getLocale()=="en"?"Prince Sultan University":"جامعة الأمير سلطان"}}</option>
                                    <option value="Fahad bin Sultan University">{{\App::getLocale()=="en"?"Fahad bin Sultan University":"جامعة الأمير فهد بن سلطان"}}</option>
                                    <option value="Prince Mohammad bin Fahd University">{{\App::getLocale()=="en"?"Prince Mohammad bin Fahd University":"جامعة الأمير محمد بن فهد"}}
                                    </option>
                                    <option value="University of Prince Mugrin">{{\App::getLocale()=="en"?"University of Prince Mugrin":"جامعة الأمير مقرن بن عبدالعزيز"}}</option>
                                    <option value="Alfaisal University">{{\App::getLocale()=="en"?"Alfaisal University":"جامعة الفيصل"}}</option>
                                    <option value="Al Yamamah University">{{\App::getLocale()=="en"?"Al Yamamah University":"جامعة اليمامة"}}</option>
                                    <option value="Dar Al Uloom University">{{\App::getLocale()=="en"?"Dar Al Uloom University":"جامعة دار العلوم"}}</option>
                                    <option value="Effat University">{{\App::getLocale()=="en"?"Effat University":"جامعة عفت"}}</option>
                                    <option value="Dar Al-Hekma University">{{\App::getLocale()=="en"?"Dar Al-Hekma University":"جامعة دار الحكمة"}}</option>
                                    <option value="Riyadh Elm University">{{\App::getLocale()=="en"?"Riyadh Elm University":"جامعة رياض العلم"}}</option>
                                    <option value="Al Maarefa University">{{\App::getLocale()=="en"?"Al Maarefa University":"جامعة المعرفة"}}</option>
                                    <option value="Mustaqbal University">{{\App::getLocale()=="en"?"Mustaqbal University":"جامعة المستقبل"}}</option>
                                    <option value="Al Asala Colleges">{{\App::getLocale()=="en"?"Al Asala Colleges":"جامعة سليمان الراجحي"}}</option>
                                    <option value="Gulf Colleges">{{\App::getLocale()=="en"?"Gulf Colleges":"كليات الخليج الأهلية"}}</option>
                                    <option value="Sulaiman Al Rajhi University">{{\App::getLocale()=="en"?"Sulaiman Al Rajhi University":"كليات عنيزة الأهلية"}}</option>
                                    <option value="Onaizah Colleges">{{\App::getLocale()=="en"?"Onaizah Colleges":"كلية ابن رشد للعلوم الإدارية"}}</option>
                                    <option value="Ibn Rushd College">{{\App::getLocale()=="en"?"Ibn Rushd College":"كلية ابن سينا الاهلية"}}</option>
                                    <option value="Ibn Sina National College">{{\App::getLocale()=="en"?"Ibn Sina National College":"كلية الباحة الاهلية للعلوم"}}</option>
                                    <option value="Al-Baha Private College of Science">{{\App::getLocale()=="en"?"Al-Baha Private College of Science":"كلية البترجي الطبية للعلوم والتكنولوجيا"}}
                                    </option>
                                    <option value="Batterjee Medical College">{{\App::getLocale()=="en"?"Batterjee Medical College":"كلية الريادة للعلوم الصحية"}}</option>
                                    <option value="Al-Riyada College for Health Science">{{\App::getLocale()=="en"?"Al-Riyada College for Health Science":"كليات الريان الأهلية بالمدينة المنورة"}}</option>
                                    <option value="Al-Rayan College">{{\App::getLocale()=="en"?"Al-Rayan College":"كليات الشرق العربي للدراسات العليا"}}</option>
                                    <option value="Arab East College">{{\App::getLocale()=="en"?"Arab East College":"كلية العناية الطبية"}}</option>
                                    <option value="Inaya Medical Colleges">{{\App::getLocale()=="en"?"Inaya Medical Colleges":"كلية الغد الدولية للعلوم الطبية التطبيقية"}}</option>
                                    <option value="Al Ghad International College for Applied Medical Science">{{\App::getLocale()=="en"?"Al Ghad International College for Applied Medical Science":"كلية الرؤية لطب الأسنان والتمريض بجدة"}}</option>
                                    <option value="Buraydah College">{{\App::getLocale()=="en"?"Buraydah College":"كليات بريدة الأهلية"}}</option>
                                    <option value="Jeddah International College">{{\App::getLocale()=="en"?"Jeddah International College":"كلية سعد للتمريض والعلوم الصحية"}}</option>
                                    <option value="Saad College of Nursing and Allied Health Sciences">{{\App::getLocale()=="en"?"Saad College of Nursing and Allied Health Sciences":"كلية فقيه للعلوم الطبية"}}</option>
                                    <option value="Fakeeh College for Medical Science">{{\App::getLocale()=="en"?"Fakeeh College for Medical Science":"كلية محمد المانع للعلوم الطبية"}}
                                    </option>
                                    <option value="Mihammad Al Mana College for Medical Science">{{\App::getLocale()=="en"?"Mihammad Al Mana College for Medical Science":"جامعة الفيصل – كلية الامير سلطان للإدارة"}}</option>
                                    <option value="Prince Sultan College of Business">{{\App::getLocale()=="en"?"Prince Sultan College of Business":"كلية الشمال للتمريض"}}
                                    </option>
                                    <option value="Vision Colleges in Jeddah">{{\App::getLocale()=="en"?"Vision Colleges in Jeddah":"كلية الرؤية بالرياض"}}</option>
                                    <option value="Vision Colleges in Riyadh">{{\App::getLocale()=="en"?"Vision Colleges in Riyadh":"كلية الفيحاء الأهلية"}}</option>
                                    <option value="Northen College of Nursing">{{\App::getLocale()=="en"?"Northen College of Nursing":"كلية الموسى للعلوم الصحية"}}</option>
                                    <option value="ALFAYHA COLLEGE">{{\App::getLocale()=="en"?"ALFAYHA COLLEGE":"كلية جدة العالمية الأهلية"}}</option>
                                    <option value="ALMOOSA COLLEGE OF HEALTH SCIENCE">{{\App::getLocale()=="en"?"ALMOOSA COLLEGE OF HEALTH SCIENCE":"اخرى"}}
                                    </option>
                                    <option value="Other">{{\App::getLocale()=="en"?"Other":""}}</option>
    
                                </select>
                                @error('university')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{ $message }}</span>
                                @enderror
                            </div>
                            @endif
                            
                            @if(\App::getLocale()=="en")
                            <div class="col-md-12">
                                <label for="">{{ __('Workshop') }} <span style="color: red;">*</span></label>
                                <select id="workshop" class="@error('workshop') is-invalid @enderror" value="{{ old('workshop') }}"
                                    name="workshop" aria-invalid="false">
                                    <option value="Select Option" selected="selected">{{__('Select Option')}}</option>
                                    <option value="Educational programs qualifying for labor market 9-10 AM">{{__('Educational programs qualifying for labor market 9-10 AM')}}</option>
                                    <option value="Educational programs qualifying for labor market 11 AM -12 PM">
                                        {{__('Educational programs qualifying for labor market 11 AM -12 PM')}}</option>
                                    <option
                                        value="The impact of vocational guidance in raising awareness of the renewable needs of labor market 9-10 AM">
                                        {{__('The impact of vocational guidance in raising awareness of the renewable needs of labor market 9-10 AM')}}</option>
                                    <option
                                        value="The impact of vocational guidance in raising awareness of the renewable needs of labor market 11 AM -12 PM">
                                        {{__('The impact of vocational guidance in raising awareness of the renewable needs of labor market 11 AM -12 PM')}}</option>
                                    <option value="Future skills and labor market 9-10 AM">{{__('Future skills and labor market 9-10 AM')}}</option>
                                    <option value="Future skills and labor market 11 AM -12 PM">{{__('Future skills and labor market 11 AM -12 PM')}}</option>
                                    <option value="Continuous learning and future changes 9-10 AM">{{__('Continuous learning and future changes 9-10 AM')}}</option>
                                    <option value="Continuous learning and future changes 11 AM -12 PM">{{__('Continuous learning and future changes 11 AM -12 PM')}}</option>
                                    <option value="Training and qualifying for labor market 10:30-11:30 AM">{{__('Training and qualifying for labor market 10:30-11:30 AM')}}</option>
                                    <option value="Training and qualifying for labor market 1:30-2:30 PM">{{__('Training and qualifying for labor market 1:30-2:30 PM')}}</option>
                                    <option value="Promoting the culture of innovation and entrepreneurship 10:30-11:30 AM">
                                        {{__('Promoting the culture of innovation and entrepreneurship 10:30-11:30 AM')}}</option>
                                    <option value="Promoting the culture of innovation and entrepreneurship 1:30-2:30 PM">
                                        {{__('Promoting the culture of innovation and entrepreneurship 1:30-2:30 PM')}}</option>
                                    <option value="Recent trends in labor market 10:30-11:30 AM">
                                        {{__('Recent trends in labor market 10:30-11:30 AM')}}</option>
                                    <option value="Recent trends in labor market 1:30-2:30 PM">
                                        {{__('Recent trends in labor market 1:30-2:30 PM')}}</option>
                                    <option value="Digital transformation and labor market 10:30-11:30 AM">{{__('Digital transformation and labor market 10:30-11:30 AM')}}</option>
                                    <option value="Digital transformation and labor market 1:30-2:30 PM">{{__('Digital transformation and labor market 1:30-2:30 PM')}}</option>
                                </select>
                                
                                @error('workshop')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{ $message }}</span>
                                @enderror
                            </div>
                            @else
                            <div class="col-md-12">
                                <label for="">{{ __('Workshop') }} <span style="color: red;">*</span></label>
                                <select id="workshop" class="@error('workshop') is-invalid @enderror" value="{{ old('workshop') }}"
                                    name="workshop" aria-invalid="false">
                                    <option value="Select Option" selected="selected">{{__('Select Option')}}</option>
                                    <option style="display: none;" value="Educational programs qualifying for labor market 9-10 AM">{{__('Educational programs qualifying for labor market 9-10 AM')}}</option>
                                    <option style="display: none;" value="Educational programs qualifying for labor market 11 AM -12 PM">
                                        {{__('Educational programs qualifying for labor market 11 AM -12 PM')}}</option>
                                    <option
                                        value="The impact of vocational guidance in raising awareness of the renewable needs of labor market 9-10 AM">
                                        {{__('The impact of vocational guidance in raising awareness of the renewable needs of labor market 9-10 AM')}}</option>
                                    <option
                                        value="The impact of vocational guidance in raising awareness of the renewable needs of labor market 11 AM -12 PM">
                                        {{__('The impact of vocational guidance in raising awareness of the renewable needs of labor market 11 AM -12 PM')}}</option>
                                    <option value="Future skills and labor market 9-10 AM">{{__('Future skills and labor market 9-10 AM')}}</option>
                                    <option style="display: none" value="Future skills and labor market 11 AM -12 PM">{{__('Future skills and labor market 11 AM -12 PM')}}</option>
                                    <option value="Continuous learning and future changes 9-10 AM">{{__('Continuous learning and future changes 9-10 AM')}}</option>
                                    <option value="Continuous learning and future changes 11 AM -12 PM">{{__('Continuous learning and future changes 11 AM -12 PM')}}</option>
                                    <option style="display: none;" value="Training and qualifying for labor market 10:30-11:30 AM">{{__('Training and qualifying for labor market 10:30-11:30 AM')}}</option>
                                    <option value="Training and qualifying for labor market 1:30-2:30 PM">{{__('Training and qualifying for labor market 1:30-2:30 PM')}}</option>
                                    <option value="Promoting the culture of innovation and entrepreneurship 10:30-11:30 AM">
                                        {{__('Promoting the culture of innovation and entrepreneurship 10:30-11:30 AM')}}</option>
                                    <option value="Promoting the culture of innovation and entrepreneurship 1:30-2:30 PM">
                                        {{__('Promoting the culture of innovation and entrepreneurship 1:30-2:30 PM')}}</option>
                                    <option value="Recent trends in labor market 10:30-11:30 AM">
                                        {{__('Recent trends in labor market 10:30-11:30 AM')}}</option>
                                    <option value="Recent trends in labor market 1:30-2:30 PM">
                                        {{__('Recent trends in labor market 1:30-2:30 PM')}}</option>
                                    <option style="display: none;" value="Digital transformation and labor market 10:30-11:30 AM">{{__('Digital transformation and labor market 10:30-11:30 AM')}}</option>
                                    <option value="Digital transformation and labor market 1:30-2:30 PM">{{__('Digital transformation and labor market 1:30-2:30 PM')}}</option>
                                </select>
                                
                                @error('workshop')
                                <span class="error-span"> <i class="fa fa-exclamation mr-2 ml-2"></i>{{ $message }}</span>
                                @enderror
                            </div>
                            @endif
                            
    
                        </div>
    <br>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-sec">
                                    <div class="header-right-btn f-right d-lg-block ml-30 submit-btn">
                                        <button type="submit" class="re-btn">{{ __('Submit') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <p class="red-line">{{__('This registration is tentative, and your attendance will be confirmed via e-mail')}}</p>
        </div>

    </div>


    <div id="bottom">
        <div class="width">
            <div class="copyrights">all rights reserved to
                <span>The First Scientific Conference For Labor Market Research 2021 </span> 2021
            </div>
            <!--End Copyright-->
            <div class="design">
            </div>
            <div style="clear:both;"></div>
            <!--End Both-->
        </div>
        <!--End Width-->
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>


    <script type='text/javascript' src='https://scrsi-sa.com/wp-includes/js/jquery/jquery.min.js?ver=3.6.0'
        id='jquery-core-js'></script>
    <script type='text/javascript' src='https://scrsi-sa.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2'
        id='jquery-migrate-js'></script>
    <script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/jquery.cycle2.js?ver=1.0.0'
        id='cycle-js'></script>
    <script type='text/javascript'
        src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/jquery.cycle2.tile.js?ver=1.0.0' id='cycle_tile-js'>
    </script>
    <script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/slick.js?ver=1.0.0'
        id='slick-js'></script>
    <script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/lightview.js?ver=1.0.0'
        id='lightview-js'></script>
    <script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/code.js?ver=1.0.0'
        id='code-js'></script>
    <script type='text/javascript' src='https://scrsi-sa.com/wp-content/themes/mo2tmr2/js/shape.js?ver=1.0.0'
        id='shape-js'></script>

    <script>
        @if(Session::has('message'))
    toastr.options = {
        "closeButton": true,
        "progressBar": true
    }
    toastr.success("{{ session('message') }}");
    @endif
    </script>

    @jquery
    @toastr_js
    @toastr_render
</body>




</html>