<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SCRSI</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Tajawal:wght@200;300&display=swap" rel="stylesheet">

</head>
<style >
    @media only screen and (max-width : 991px) {
        .main-div {
            width: 80% !important;
        }
    }
</style>

<body style="margin: 0;">
    <div
        style="padding-top: 55px; background-image: url('https://imgur.com/rMXg6Xs.png'); height: 100%; background-repeat: no-repeat;    background-size: cover;">
        <div class="main-div"
            style="width: 400px; margin: auto; box-shadow: 0px 0px 15px 2px #0000001f; background: white;">

            <table style="width: 100%; margin: auto;border-spacing: 0;" border="0" cellpsacing="0" bor>
                <tr>
                    <td colspan="2"  style=" padding: 5px;"> <img
                            style="    display: block;margin: auto; max-width: 100%;"
                            src="https://scrsi-sa.com/wp-content/uploads/2021/10/logo10.png" alt="" width="300"> </td>
                   
                </tr>
                <tr>
                    <td colspan="2"
                        style="padding: 20px; text-align: center; font-family: arial; padding-top: 40px;font-family: 'Tajawal', sans-serif;color: black;">
                        Thank you for registering in The First Scientific Conference on Labor Market Research, Studies, and Indicators.
                    </td>
                </tr>
                <tr>
                    <td colspan="2"
                        style="padding: 20px; text-align: center; font-family: arial; padding-top: 0px;font-family: 'Tajawal', sans-serif;">
                        شكرا لتسجيلك في المؤتمر العلمي الأول لبحوث ودراسات ومؤشرات سوق العمل
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <img width="120" style="width: 140px;margin: auto;display: block; margin-bottom: 30px;"
                        src="{{asset('storage/qr-code/'.$qrcodeid.'.png')}}"
                            alt="">
                    </td>
                </tr>

            </table>
            <table style="width: 100%; margin: auto;border-spacing: 0; border: 0;">
                <tr style="background-color: #57b89a;">
                    <td colspan="3" style="text-align: center;">
                        <p
                        style="text-align: center; margin-top: 10px; margin-bottom: 10px; font-family: arial;padding-bottom: 10px;font-family: 'Tajawal', sans-serif; color: white;">
                        ALL Right Reserved</p>
                    </td>
                    
                </tr>
            </table>
        </div>
        
    </div>

</body>

</html>