<?php

namespace App\Http\Controllers;

use App\Models\Register;
use App\Models\Workshop;
use Illuminate\Http\Request;

class ApiController extends Controller
{
     public function userdetail($qrcodeid){
        $response=[];
        $msg=[];
        // return response()->json(['error'=>"user Found"], 200);
        $userdetail = Register::where('qrcodeid', $qrcodeid)->first();
        
        if(!isset($userdetail)){

        $msg[]='User Not Found';
        return response()->json([
            'msg' => $msg,
        ],404); 
        }
           $msg[]='User Found';
        $response[]=$userdetail;
           return response()->json([
            'result' => $response,
            'msg'=> $msg,
        ],200);
        
            
    }
     public function workshopdetail($qrcodeid){
        $response=[];
        $msg=[];
        // return response()->json(['error'=>"user Found"], 200);
        $workshopdetail = Workshop::where('qrcodeid', $qrcodeid)->first();
        
        if(!isset($workshopdetail)){

        $msg[]='Workshop Not Found';
        return response()->json([
            'msg' => $msg,
        ],404); 
        }
           $msg[]='Workshop Found';
        $response[]=$workshopdetail;
           return response()->json([
            'result' => $response,
            'msg'=> $msg,
        ],200);
        
            
    }
    public function workshopqrscan($workshopid){
       
        $msg=[];
        $workshop = Workshop::find($workshopid);
        $workshop->qrcodestatus = "1";
        $workshop->save();
        
        if(!isset($workshop)){

            $msg[]='Workshop Not Found';
            return response()->json([
                'msg' => $msg,
            ],404); 
            }
        $msg[]='Status Changed';
           return response()->json([
            'msg'=> $msg,
        ],200);
    }
    public function qrscan($userid){
       
        $msg=[];
        $user = Register::find($userid);
        $user->qrcodestatus = "1";
        $user->save();
        
        if(!isset($user)){

            $msg[]='User Not Found';
            return response()->json([
                'msg' => $msg,
            ],404); 
            }
        $msg[]='Status Changed';
           return response()->json([
            'msg'=> $msg,
        ],200);
    }
}
