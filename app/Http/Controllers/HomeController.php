<?php

namespace App\Http\Controllers;
// use Illuminate\Http\Request;

use App\Models\Female;
use App\Models\Male;
use App\Models\Register;
use App\Models\Survey;
use App\Models\Workshop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use PDF;

class HomeController extends Controller
{
    public function showregister()
    {
        return view('register');
    }

    public function storeregister(Request $request)
    {
        $randomNumber = random_int(100000, 999999);
        $this->validate($request, [
            'name' => 'required',
            'jobtitle' => 'required',
            'email' => 'required|email|unique:registers',
            'organization' => 'required',
            'phonenumber' => 'required|max:15|regex:/^([0-9\+]*)$/',

            // 'c_password' => 'required|same:password',
        ]);
        $data = new Register();
        $data->name = $request->name;
        $data->jobtitle = $request->jobtitle;
        $data->email = $request->email;
        $data->organization = $request->organization;
        $data->phonenumber = $request->phonenumber;
        $data->qrcodeid = $randomNumber;
        $data->save();
        $image = QrCode::format('png')
            ->size(200)->errorCorrection('H')
            ->generate($data->qrcodeid);
        $output_file = 'public/qr-code/' . $data->qrcodeid . '.png';
        Storage::disk('local')->put($output_file, $image);

        $details = [
            'qrcodeid' => $data->qrcodeid,
        ];

        \Mail::to($request->email)->send(new \App\Mail\RegisterMail($details));
        if (\App::getLocale() == "en") {
            toastr()->success('You are Successfully Registered');
            return redirect()->route('home');
        }
        toastr()->success('تم التسجيل بنجاح');
        return redirect()->route('home');
    }
    public function langSwitch()
    {
        if (\Session::has('lang') && \Session::get('lang') == 'en') {
            \Session::put('lang', 'ar');
        } else {
            \Session::put('lang', 'en');
        }

        // dd(\App::getLocale());

        return back();
    }
    public function showworkshop()
    {
        // dd(\App::getLocale());
        return view('workshop');
    }
    public function storeworkshop(Request $request)
    {
        // dd(\Session::get('lang'));
// dd(\App::getLocale());
        $randomNumber = random_int(100000, 999999);
        $this->validate($request, [
            'name' => 'required',
            'studentstatus' => 'required',
            'email' => 'required|email|unique:workshops',
            'university' => 'required',
            'workshop' => 'required',

            // 'c_password' => 'required|same:password',
        ]);
        $data = new Workshop();
        $data->name = $request->name;
        $data->studentstatus = $request->studentstatus;
        $data->email = $request->email;
        $data->university = $request->university;
        $data->workshop = $request->workshop;
        $data->qrcodeid = $randomNumber;
        $data->save();
        $image = QrCode::format('png')
            ->size(200)->errorCorrection('H')
            ->generate($data->qrcodeid);
        $output_file = 'public/qr-code/workshop' . $data->qrcodeid . '.png';
        Storage::disk('local')->put($output_file, $image);

        // $details = [
        //     'qrcodeid' => $data->qrcodeid,
        // ];
        // \Mail::to($request->email)->send(new \App\Mail\RegisterMail($details));
        // $lang = $request->query();
        // dd($lang);
            // dd(\App::getLocale());
        // dd(\App::getLocale());

        if (\App::getLocale() == "en") {
            // dd("english m hain");
            toastr()->success('Workshop Successfully Registered');
            return redirect()->route('workshop');

        }
        // dd(\App::getLocale());

        toastr()->success('تم التسجيل بنجاح');
        return redirect()->route('workshop');
    }
    // function generateAllQr(){
    //     $workshop = DB::table('workshops')->whereBetween('id', [2401, 2500])->get();
    //         // dd($workshop);
    //     // $workshop = Workshop::all();
    //     foreach($workshop as $workshop){
    //         // dd($workshop->qrcodeid);
    //         $image = QrCode::format('png')
    //         ->size(200)->errorCorrection('H')
    //         ->generate($workshop->qrcodeid);
    //     $output_file = 'public/qr-code/workshop'.$workshop->qrcodeid.'.png';
    //     Storage::disk('local')->put($output_file, $image);
    //     }
        
    //     dd("all images are generated 2401, 2500");
    // }
    public function Survey(){
        return view('survey');
    }
    public function Store_Survey(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'gender' => 'required',
            'questionone' => 'required',
            'questiontwo' => 'required',
            'questionthree' => 'required',
            'questionfour' => 'required',
            'questionfive' => 'required',
            'questionsix' => 'required',
            'questionseven' => 'required',
            'questioneight' => 'required',
            'questionnine' => 'required',
            'questionten' => 'required',
            'questioneleven' => 'required',
            'questiontwelve' => 'required',
            'questionthirteen' => 'required',
            'questionfourteen' => 'required',
            'questionfifteen' => 'required'
            
        ]);
        $surveydata = new Survey();
        $surveydata->name = $request->name;
        $surveydata->gender = $request->gender;
        $surveydata->questionone = $request->questionone;
        $surveydata->questiontwo = $request->questiontwo;
        $surveydata->questionthree = $request->questionthree;
        $surveydata->questionfour = $request->questionfour;
        $surveydata->questionfive = $request->questionfive;
        $surveydata->questionsix = $request->questionsix;
        $surveydata->questionseven = $request->questionseven;
        $surveydata->questioneight = $request->questioneight;
        $surveydata->questionnine = $request->questionnine;
        $surveydata->questionten = $request->questionten;
        $surveydata->questioneleven = $request->questioneleven;
        $surveydata->questiontwelve = $request->questiontwelve;
        $surveydata->questionthirteen = $request->questionthirteen;
        $surveydata->questionfourteen = $request->questionfourteen;
        $surveydata->questionfifteen = $request->questionfifteen;
        $surveydata->save();
        $name = $surveydata->name;
        $gender = $surveydata->gender;
        $id = $surveydata->id;
        // dd($id);
        return view('aftersurvey',compact('name','gender','id'));
    }
    public function pdfview(Request $request)  
    {
        set_time_limit(0);
        $data = Survey::select('name','gender')->where('id',$request->id)->first();
        // // // dd($name->name);
        // // view()->share(['name'=>$data->name,'gender'=>$data->gender,
        // // 'id'=> $request->id]);
        // // $viewdata = [
        // //     'name'=>$data->name,
        // //     'gender'=>$data->gender,
        // //     'id'=> $request->id,
        // // ];
        $name = $data->name;
        $gender = $data->gender;
        $id = $request->id;
        if($request->has('download')){ 
            $view = View::make('certificatepdf',compact('name','gender'));
            $content = $view->render();


            $pdf = new \Mpdf\Mpdf(['mode'=>'UTF-8','format'=>[279.4, 215.9]]);
            $pdf->AddPage();
            // // $pdf->SetFont('pmFont');
            $pdf->writeHTML($content);
            $pdf->Output('Certificate.pdf','D');
            
        }  

  
        return view('aftersurvey',compact('name','gender','id'));
    }
    public function Certificate(){
    return view('workshopcertificateview');
    }
    public function MaleCertificate($email){
        set_time_limit(0);
        
        // dd($email);
        // $username = User::select('name')->where('email',$email)->first(); 
        $data =Male::select('name','workshop','day','date')->where('email',$email)->first();
        if(!isset($data)){
            return view('NotFoundRecord');
        }
        $name = $data->name;
        $workshopname = $data->workshop;
        $date = $data->date;
        $day = $data->day;
        
        // if($request->has('download')){ 
            $view = View::make('maleworkshopcertificate',compact('name','workshopname','day','date'));
            $content = $view->render();


            $pdf = new \Mpdf\Mpdf(['mode'=>'UTF-8','format'=>[252.4, 230]]);
            $pdf->AddPage();
            // // $pdf->SetFont('pmFont');
            $pdf->writeHTML($content);
            $pdf->autoScriptToLang = true;
            $pdf->autoLangToFont = true;
            $pdf->Output('Certificate.pdf','D');
            
        // }  

  
        return view('maleworkshopcertficate',compact('name','workshopname','day','date'));
    }
    public function FemaleCertificate($email){
        set_time_limit(0);
        
        // dd($email);
        // $username = User::select('name')->where('email',$email)->first(); 
        $data =Female::select('name','workshop','day','date')->where('email',$email)->first();
        if(!isset($data)){
            return view('NotFoundRecord');
        }
        $name = $data->name;
        $workshopname = $data->workshop;
        $date = $data->date;
        $day = $data->day;
        
        // if($request->has('download')){ 
            $view = View::make('femaleworkshopcertificate',compact('name','workshopname','date','day'));
            $content = $view->render();


            $pdf = new \Mpdf\Mpdf(['mode'=>'UTF-8','format'=>[252.4, 230]]);
            $pdf->AddPage();
            // // $pdf->SetFont('pmFont');
            $pdf->writeHTML($content);
            $pdf->autoScriptToLang = true;
            $pdf->autoLangToFont = true;
            $pdf->Output('Certificate.pdf','D');
            
        // }  

  
        return view('femaleworkshopcertficate',compact('name','workshopname','date','day'));
    }
}
