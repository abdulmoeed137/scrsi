<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class AdminController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectto='/admin';

    function __construct(){
        $this->middleware('guest:admin')->except('logout');
    }

    function ShowLogin(){
        return view('Admin/Auth/login');
    }
    function login(Request $request){
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required',
        ]);
        // dd($request);
        // $credentials = $request->only('email','password');
        // dd(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password]));
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])){
            // dd('we are going to admin dashboard');
            return redirect()->intended(route('admindashboard'));
        }
        toastr()->error('Invalid Credentials');

        return redirect()->route('adminlogin');
    }
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/admin/login');
      }

   

    protected function guard()
    {
        return Auth::guard('admin');
    }

}
