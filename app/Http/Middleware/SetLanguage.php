<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // $lang='';
// dd($lang);
        $lang = $request->query('lang');
      
// dd($lang);

        if ($lang == 'en') {
            \Session::put('lang', 'en');
        } else if($lang == 'ar') {
            \Session::put('lang', 'ar');
        }
       
        if(\Session::has('lang')){
            $lang=\Session::get('lang');
        }else{
            if ($lang == null) $lang = 'ar';
            \Session::put('lang',$lang);
        }
      
        // dd($lang);
        \App::setLocale($lang);
        // dd(\App::getLocale());
        return $next($request);
    }
}
