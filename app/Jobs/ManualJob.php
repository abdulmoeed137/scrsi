<?php

namespace App\Jobs;

use App\Models\Workshop;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ManualJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $arr1;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($arr1)
    {
        $this->arr1 = $arr1;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // dd($this->arr1);
        $manualworkshop = Workshop::whereIn('id', $this->arr1)->get();
        //  dd($manualworkshop);
         foreach($manualworkshop as $manualworkshop){
            dd($manualworkshop->email);

         }
        
        // $data = array('subject'=>$template->subject,'content'=>$template->content);
        \Mail::send('Admin.emails.manualtemplate',function($message) use ($manualworkshop){
            foreach($manualworkshop as $manualworkshop){
                $message->to($manualworkshop->email);
            }
        });
    }
}
