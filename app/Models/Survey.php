<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    use HasFactory;
    protected $fillable=["name","gender","questionone","questiontwo","questionthree","questionfour","questionfive",
 "questionsix","questionseven","questioneight","questionnine","questionten","questioneleven","questiontwelve","questionthirteen",
"questionfourteen","questionfifteen"];
}
